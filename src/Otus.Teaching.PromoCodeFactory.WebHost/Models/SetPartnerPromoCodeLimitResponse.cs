﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class SetPartnerPromoCodeLimitResponse
    {
        public Guid Id { get; set; }
        public Guid LimitId { get; set; }
    }
}
