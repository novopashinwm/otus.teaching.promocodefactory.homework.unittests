﻿using AutoFixture;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Microsoft.AspNetCore.Mvc;


namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private Fixture _fixture;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _fixture = new Fixture();
            _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList().ForEach(b => _fixture.Behaviors.Remove(b));
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
        }

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404
        /// </summary>
        /// <returns>404</returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerNotFound_Return404() 
        {
            //Arrange
            var id = Guid.NewGuid();
            var partnersController = BuildPartnersControllerMock(r => r.GetByIdAsync(It.IsAny<Guid>()), null);
            var setPartnerPromoCodeLimitRequest = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            //Act
            var result = (IStatusCodeActionResult)await partnersController
                .SetPartnerPromoCodeLimitAsync(id, setPartnerPromoCodeLimitRequest);

            //Assert
            result.StatusCode.Should().NotBeNull().And.Be(404);

        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerNotActive_Return400()
        {
            //Arrange
            var partner = _fixture.Build<Partner>()
                .With(p => p.IsActive, false)
                .Create();
            var partnersController = BuildPartnersControllerMock(r => r.GetByIdAsync(partner.Id), partner);
            var setPartnerPromoCodeLimitRequest = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            //Act
            var result = (IStatusCodeActionResult)await partnersController
                .SetPartnerPromoCodeLimitAsync(partner.Id, setPartnerPromoCodeLimitRequest);

            //Assert
            result.StatusCode.Should().NotBeNull().And.Be(400);
        }

        /// <summary>
        /// Если партнеру выставляется лимит и лимит не закончился, 
        /// то мы должны обнулить количество промокодов, 
        /// которые партнер выдал NumberIssuedPromoCodes.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerLimitSetAndLimitNotExpired_ResetNumberIssuedPromoCodes()
        {
            //Arrange
            var partnersLimit = _fixture.Build<PartnerPromoCodeLimit>()
                .With(p => p.CancelDate, (DateTime?)null)
                .With(p => p.EndDate, DateTime.Now.AddDays(10))
                .Create();
            var partner = _fixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .With(p => p.NumberIssuedPromoCodes, 1000)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit> { partnersLimit })
                .Create();
            var partnersController = BuildPartnersControllerMock(r => r.GetByIdAsync(partner.Id), partner);
            var notExpiredPartnerPromoCodeLimitRequest = _fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(s => s.Limit, 100)
                .Create();

            //Act
            var result = (IStatusCodeActionResult)await partnersController
                .SetPartnerPromoCodeLimitAsync(partner.Id, notExpiredPartnerPromoCodeLimitRequest);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
            result.StatusCode.Should().Be(201);
        }

        /// <summary>
        /// Если партнеру выставляется лимит и лимит закончился, то мы НЕ должны обнулять количество промокодов, 
        /// которые партнер выдал NumberIssuedPromoCodes.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerLimitSetAndLimitExpired_NotResetNumberIssuedPromoCodes()
        {
            //Arrange
            var partnersLimit = _fixture.Build<PartnerPromoCodeLimit>()
                .With(p => p.CancelDate, (DateTime?)null)
                .With(p => p.EndDate, DateTime.Now.AddDays(-10))
                .Create();
            var partner = _fixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .With(p => p.NumberIssuedPromoCodes, 1000)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit> { partnersLimit })
                .Create();
            var partnersController = BuildPartnersControllerMock(r => r.GetByIdAsync(partner.Id), partner);
            var notExpiredPartnerPromoCodeLimitRequest = _fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(s => s.Limit, 100)
                .Create();

            //Act
            var result = (IStatusCodeActionResult)await partnersController
                .SetPartnerPromoCodeLimitAsync(partner.Id, notExpiredPartnerPromoCodeLimitRequest);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(1000);
            result.StatusCode.Should().Be(201);
        }

        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerLimitSet_DisablePreviousLimit()
        {
            //Arrange
            var partnersLimit = _fixture.Build<PartnerPromoCodeLimit>()
                .With(p => p.CancelDate, (DateTime?)null)
                .Create();
            var partner = _fixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .With(p => p.NumberIssuedPromoCodes, 1000)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit> { partnersLimit })
                .Create();
            var partnersController = BuildPartnersControllerMock(r => r.GetByIdAsync(partner.Id), partner);
            var notExpiredPartnerPromoCodeLimitRequest = _fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(s => s.Limit, 100)
                .Create();

            //Act
            var result = (IStatusCodeActionResult)await partnersController
                .SetPartnerPromoCodeLimitAsync(partner.Id, notExpiredPartnerPromoCodeLimitRequest);

            //Assert
            partnersLimit.CancelDate.Should().NotBeNull();
            result.StatusCode.Should().Be(201);
        }

        /// <summary>
        /// Лимит должен быть больше 0;
        /// </summary>
        /// <returns></returns>
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(int.MinValue)]
        public async Task SetPartnerPromoCodeLimitAsync_RequestLimitLessOrEqualsZero_Return400(int requestLimit)
        {
            //Arrange
            var partner = _fixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .Create();
            var partnersController = BuildPartnersControllerMock(r => r.GetByIdAsync(partner.Id), partner);
            var notExpiredPartnerPromoCodeLimitRequest = _fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(s => s.Limit, requestLimit)
                .Create();

            //Act
            var result = (IStatusCodeActionResult)await partnersController
                .SetPartnerPromoCodeLimitAsync(partner.Id, notExpiredPartnerPromoCodeLimitRequest);

            //Assert
            result.StatusCode.Should().Be(400);
        }

        /// <summary>
        /// Новый лимит должен быть сохранён в базу данных.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_NewLimitMustBeSaved_CheckInDb()
        {
            //Arrange
            var serviceProvider = BuildServiceCollectionWithEfInMemory();
            var partner = _fixture.Build<Partner>()
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>())
                .With(p => p.IsActive, true)
                .Create();
            var partnerPromoCodeLimitRequest = _fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(s => s.Limit, 55)
                .Create();
            var partnerRepository = serviceProvider.GetRequiredService<IRepository<Partner>>();
            await partnerRepository.AddAsync(partner);

            var partnersController = new PartnersController(serviceProvider.GetRequiredService<IRepository<Partner>>());

            //Act
            var result = (CreatedAtActionResult)await partnersController
                .SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromoCodeLimitRequest);

            var resultPartner = await partnerRepository.GetByIdAsync(partner.Id);

            //Assert
            result.StatusCode.Should().Be(201);
            result.RouteValues[nameof(SetPartnerPromoCodeLimitResponse.LimitId)]
                .Should().NotBeNull()
                .And.Be(resultPartner.PartnerLimits.Single().Id);

            result.RouteValues[nameof(SetPartnerPromoCodeLimitResponse.Id)]
                .Should().NotBeNull()
                .And.Be(resultPartner.Id);

            resultPartner.PartnerLimits.Should().ContainSingle(x => x.PartnerId == partner.Id)
                .And.ContainSingle(x => x.Limit == partnerPromoCodeLimitRequest.Limit)
                .And.ContainSingle(x => x.EndDate == partnerPromoCodeLimitRequest.EndDate);
        }

        private IServiceProvider BuildServiceCollectionWithEfInMemory()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddDbContext<DataContext>(cfg => cfg.UseInMemoryDatabase("InMemoryTestDb"));
            serviceCollection.AddTransient(typeof(IRepository<>), typeof(EfRepository<>));
            return serviceCollection.BuildServiceProvider();
        }


        private PartnersController BuildPartnersControllerMock(Expression<Func<IRepository<Partner>, Task<Partner>>> setup, Partner returnsValue)
        {
            var mock = new Mock<IRepository<Partner>>();
            mock.Setup(setup).ReturnsAsync(returnsValue);
            return new PartnersController(mock.Object);
        }

    }
}